# IKOA Business Opportunity Core API

Utility functions for basic development. This library is part of IKOA Business Opportunity Microservices Infraestructure.

[![Version npm](https://img.shields.io/npm/v/@ikoabo/core.svg?style=flat-square)](https://www.npmjs.com/package/@ikoabo/core)[![npm Downloads](https://img.shields.io/npm/dm/@ikoabo/core.svg?style=flat-square)](https://npmcharts.com/compare/@ikoabo/core?minimal=true)[![Build Status](https://gitlab.com/ikoabo/packages/core/badges/master/pipeline.svg)](https://gitlab.com/ikoabo/packages/core)[![coverage testing report](https://gitlab.com/ikoabo/packages/core/badges/master/coverage.svg)](https://gitlab.com/ikoabo/packages/core/-/commits/master)

[![NPM](https://nodei.co/npm/@ikoabo/core.png?downloads=true&downloadRank=true)](https://nodei.co/npm/@ikoabo/core/)

## Installation

```bash
npm install @ikoabo/core
```

## Predefined constants

Package include a set of predefined constants to be used inside backend/frontend development.
It includes constants to prefeined object status, prefined general errors, logs level, and HTTP status responses.

```js
import { LOG_LEVEL, SERVER_STATUS, SERVER_ERRORS, HTTP_STATUS } from "@ikoabo/core";
```

## Using Logger

Logger is an small wrapper of [`winston`][winston] logger. It only hande logs to the console
output and must be configured on server initialization. Logger support the same log levels of
[`winston`][winston].

```js
import { Logger, LOG_LEVEL } from "@ikoabo/core";

/* Set the global log level */
Logger.setLogLevel(LOG_LEVEL.DEBUG);

/* Initialize the logger for multiple components */
const _logger1 = new Logger("MiComponent");
const _logger2 = new Logger("OtherComponent");

/* Log an error from one logger */
_logger1.error("Error from one component", {
  code: 2,
  msg: "Invalid call"
});

/* Log a debug message from the other logger */
_logger2.debug("Debug from another component", {
  field: "social",
  value: 10
});
```

## Using Arrays utilities functions

Arrays implements functions to improve array data manipulation. it implements functions to ensure array initialization with include/exclude values, array sort, binary search and multiple arrays intersection.

```js
import { Arrays } from "@ikoabo/core";
let arr1 = [1, 2, 3, 5, 7];
let arrInclude = [3, 15, 6];
let arrExclude = [2, 5];

/* Get new array [1, 3, 7, 15, 6] */
let newArr = Arrays.initialize < number > (arr1, arrInclude, arrExclude);
console.log(newArr);

/* Sort the array and search a value inside the array */
Arrays.sort < number > newArr;
console.log(Arrays.search(newArr, 7)); // Prints 3

/* Intersect multiple arrays, gets [3] */
let intArr = Arrays.intersect < number > (newArr, arr1, arrInclude);
console.log(intArr);
```

## Using Objects utilities functions

Objects utilities functions allow to fetch object properties and set
a default value if any path don't exists.

```js
import { Objects } from "@ikoabo/core";

let obj = {
  alfa: {
    profiles: [
      {
        name: "Jhon",
        age: 25
      }
    ]
  }
};

/* Print Jhon */
console.log(Objects.get(obj, "alfa.profiles.0.name", "no-name"));

/* Try to get non existent property */
if (!Objects.get(obj, "alfa.profiles.0.social.facebook")) {
  console.log("Facebook not configured");
}
```

Also functions allow to set an object value following the geiven path.
If any elements inside path don't exists then it's created.

```js
import { Objects } from "@ikoabo/core";

let obj = {
  alfa: {
    profiles: [
      {
        name: "Jhon",
        age: 25
      }
    ]
  }
};

/* Update property */
Objects.set(obj, "alfa.profiles.0.name", "Jhon Doe");
console.log(Objects.get(obj, "alfa.profiles.0.name"));

/* Set non existent property */
Objects.set(obj, "alfa.profiles.0.social.facebook.profile", "facebookid");
console.log(Objects.get(obj, "alfa.profiles.0.social.facebook.profile"));
```

## Using Tokens utilities functions

Tokens its a set of function to generate pseudorandoms tokens. There are functions to generate
short, medium and long tokens. Short and medium token are generated with [`uniqid`][uniqid] and long tokens are generated with [`sha.js`][sha.js].

```js
import { Tokens } from "@ikoabo/core";

/* Generate short token (8 byte) */
const shortToken = Tokens.short;

/* Generate medium token */
const mediumToken1 = Tokens.medium1; // 12 bytes
const mediumToken2 = Tokens.medium2; // 18 bytes

/* Generate long token with sha256 */
const longToken = Tokens.long;
```

## Using Streams

Stream class allow to pipe streamed data to the `express` response. User can use a filter function to prepare the object data to be sent into the response. Filter function its an optional parameter.

```js
import { Streams } from "@ikoabo/core";

...

router.get("/data",
  (req: Request, res: Response, _next: NextFunction) => {
    MongoModel.find({ ... }).cursor().pipe(Streams.stringify((data: any)=>{
      return {
        id: data.id,
        name: data.name
      };
    })).pipe(res.type("json"));
  },
  ResponseHandler.success,
  ResponseHandler.error
);
```
