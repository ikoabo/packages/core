# CHANGELOG

## [1.1.4] - 2021-06-17

- Change error definition to allow error description

## [1.1.2] - 2021-04-21

- Fix through dependency

## [1.1.1] - 2021-04-21

- Add unit testing for streams
- Update package versions

## [1.1.0] - 2020-12-26

- Adding data streaming function
- Update package dependencies

## [1.0.6] - 2020-09-03

- Fix package engine restriction
- Update package dependencies
- Update eslint configuration

## [1.0.5] - 2020-08-16

- Fix package license
- Update package keywords

## [1.0.4] - 2020-08-16

- Remove deprecated sha256 dependency
- Update token utilities functions with sha.js dependency
- Updating package to MIT license
- Adding README documentation

## [1.0.3] - 2020-08-16

- Initial public version
- Predefined constants
- Winston logger controller
- Token utilities functions
- Arrays utilities functions
- Objects utilitites functions
