/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "mocha";
import chai from "chai";

import { Arrays } from "../src/index";
const expect = chai.expect;

describe("Array utility functions", () => {
  it("Initialize array", (done) => {
    const initial: string[] = ["one", "two", "four", "five", "six"];
    const defaults: string[] = ["six", "seven"];
    const prevents: string[] = ["two", "five", "ten"];
    const result = Arrays.initialize<string>(initial, defaults, prevents);
    const result2 = Arrays.initialize<string>(null, defaults, null);
    const result3 = Arrays.initialize<string>(initial, null, prevents);
    expect(result).to.be.a("array").to.eql(["one", "four", "six", "seven"]);
    expect(result2).to.be.a("array").to.eql(["six", "seven"]);
    expect(result3).to.be.a("array").to.eql(["one", "four", "six"]);
    done();
  }).timeout(1000);

  it("Sort array of values", (done) => {
    const initial: number[] = [1, 7, 4, 9, 3, 4, 2];
    Arrays.sort<number>(initial);
    expect(initial).to.be.a("array").to.eql([1, 2, 3, 4, 4, 7, 9]);
    done();
  }).timeout(1000);

  it("Search array value", (done) => {
    const initial: number[] = [1, 2, 3, 4, 4, 7, 9];
    const idx1 = Arrays.search<number>(initial, 3);
    const idx2 = Arrays.search<number>(initial, 4);
    const idx3 = Arrays.search<number>(initial, 7);
    expect(idx1).to.be.a("number").to.be.eq(2);
    expect(idx2).to.be.a("number").to.be.eq(3);
    expect(idx3).to.be.a("number").to.be.eq(5);
    done();
  }).timeout(1000);

  it("Intersects arrays", (done) => {
    const arr1: string[] = ["one", "two", "three", "four"];
    const arr2: string[] = ["three", "two", "four"];
    const arr3: string[] = ["four", "three"];
    const strResult = Arrays.intersect<string>(arr1, arr2, arr3);
    expect(strResult).to.be.a("array").to.eql(["four", "three"]);
    done();
  }).timeout(1000);
});
