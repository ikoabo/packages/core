/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "mocha";
import chai from "chai";

import { Tokens } from "../src/index";
const expect = chai.expect;

describe("Token utility functions", () => {
  it("Generate short (8 byte) token", (done) => {
    const token = Tokens.short;
    expect(token).to.be.a("string").to.have.length(8);
    done();
  }).timeout(1000);

  it("Generate medium (12 byte) token", (done) => {
    const token = Tokens.medium1;
    expect(token).to.be.a("string");
    done();
  }).timeout(1000);

  it("Generate medium (16 byte) token", (done) => {
    const token = Tokens.medium2;
    expect(token).to.be.a("string");
    done();
  }).timeout(1000);

  it("Generate long token", (done) => {
    const token = Tokens.long;
    expect(token).to.be.a("string").to.have.length(64);
    done();
  }).timeout(1000);

  it("Different short (8 byte) token", (done) => {
    const token1 = Tokens.short;
    const token2 = Tokens.short;
    expect(token1).to.be.a("string").to.have.length(8);
    expect(token2).to.be.a("string").to.have.length(8);
    expect(token1).to.not.be.eq(token2);
    done();
  }).timeout(1000);

  it("Different medium (11 byte) token", (done) => {
    const token1 = Tokens.medium1;
    const token2 = Tokens.medium1;
    expect(token1).to.be.a("string");
    expect(token2).to.be.a("string");
    expect(token1).to.not.be.eq(token2);
    done();
  }).timeout(1000);

  it("Different medium (16 byte) token", (done) => {
    const token1 = Tokens.medium2;
    const token2 = Tokens.medium2;
    expect(token1).to.be.a("string");
    expect(token2).to.be.a("string");
    expect(token1).to.not.be.eq(token2);
    done();
  }).timeout(1000);

  it("Different long token", (done) => {
    const token1 = Tokens.long;
    const token2 = Tokens.long;
    expect(token1).to.be.a("string").to.have.length(64);
    expect(token2).to.be.a("string").to.have.length(64);
    expect(token1).to.not.be.eq(token2);
    done();
  }).timeout(1000);
});
