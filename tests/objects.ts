/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "mocha";
import chai from "chai";

import { Objects } from "../src/index";
const expect = chai.expect;

describe("Object utility functions", () => {
  it("Get nested value", (done) => {
    const tmp: any = {
      a: {
        b: {
          c: {
            d: 4,
            e: "Alfa"
          }
        },
        f: "Beta"
      }
    };
    expect(Objects.get(tmp, "a.b.c.d")).to.be.a("number").to.be.eq(4);
    expect(Objects.get(tmp, "a.b.c.e")).to.be.a("string").to.be.eq("Alfa");
    expect(Objects.get(tmp, "a.f")).to.be.a("string").to.be.eq("Beta");
    expect(Objects.get(null, "a.f", "Zeta")).to.be.a("string").to.be.eq("Zeta");
    expect(Objects.get(tmp, null, "Zeta")).to.be.a("object").to.be.eq(tmp);
    expect(Objects.get(tmp, "a.z")).to.be.undefined;
    done();
  }).timeout(1000);

  it("Get array value", (done) => {
    const tmp: any = {
      a: {
        b: {
          c: {
            d: ["Alfa", { alfa: "Beta" }]
          }
        }
      }
    };
    expect(Objects.get(tmp, "a.b.c.d.0")).to.be.a("string").to.be.eq("Alfa");
    expect(Objects.get(tmp, "a.b.c.d.1.alfa")).to.be.a("string").to.be.eq("Beta");
    done();
  }).timeout(1000);

  it("Set nested value", (done) => {
    const tmp: any = {};
    Objects.set(tmp, "a.b.c.d", 4);
    Objects.set(tmp, "a.b.c.e", "Alfa");
    Objects.set(tmp, "a.f", "Beta");
    Objects.set(tmp, null, "Beta");
    expect(Objects.get(tmp, "a.b.c.d")).to.be.a("number").to.be.eq(4);
    expect(Objects.get(tmp, "a.b.c.e")).to.be.a("string").to.be.eq("Alfa");
    expect(Objects.get(tmp, "a.f")).to.be.a("string").to.be.eq("Beta");
    done();
  }).timeout(1000);

  it("Set array value", (done) => {
    const tmp: any = {};
    Objects.set(tmp, "a.b.c.d", []);
    Objects.set(tmp, "a.b.c.d.0", "Alfa");
    Objects.set(tmp, "a.b.c.d.1.alfa", "Beta");
    expect(Objects.get(tmp, "a.b.c.d.0")).to.be.a("string").to.be.eq("Alfa");
    expect(Objects.get(tmp, "a.b.c.d.1.alfa")).to.be.a("string").to.be.eq("Beta");
    done();
  }).timeout(1000);
});
