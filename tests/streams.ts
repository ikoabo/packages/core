/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "mocha";
import chai from "chai";
import Stream from "stream";
import { Streams } from "../src/index";
const expect = chai.expect;
const items = [
  {
    id: 1,
    name: "John Doe",
    country: "Ecuador"
  },
  {
    id: 2,
    name: "Jane Doe",
    country: "Chile"
  }
];

describe("Stream api", () => {
  it("Default stream", (done) => {
    const expectedBuffer = Buffer.from(JSON.stringify(items));
    let bytes = Buffer.from("");

    /* Prepare the stream */
    const readable = new Stream.Readable({ objectMode: true });

    /* Emit data to the stream */
    items.forEach((item) => readable.push(item));

    /* Finish data emit */
    readable.push(null);
    readable
      .pipe(Streams.stringify())
      .on("data", (chunk: any) => {
        const chunkBuff = Buffer.from(chunk);
        bytes = Buffer.concat([bytes, chunkBuff]);
      })
      .on("end", () => {
        try {
          expect(bytes).to.deep.equal(expectedBuffer);
          done();
        } catch (err) {
          done(err);
        }
      });
  }).timeout(1000);

  it("Filtered object fields stream", (done) => {
    const expectedBuffer = Buffer.from(
      JSON.stringify(
        items.map((value: any) => {
          return { name: value.name };
        })
      )
    );
    let bytes = Buffer.from("");

    /* Prepare the stream */
    const readable = new Stream.Readable({ objectMode: true });

    /* Emit data to the stream */
    items.forEach((item) => readable.push(item));

    /* Finish data emit */
    readable.push(null);
    readable
      .pipe(
        Streams.stringify((data: any) => {
          return { name: data.name };
        })
      )
      .on("data", (chunk: any) => {
        const chunkBuff = Buffer.from(chunk);
        bytes = Buffer.concat([bytes, chunkBuff]);
      })
      .on("end", () => {
        try {
          expect(bytes).to.deep.equal(expectedBuffer);
          done();
        } catch (err) {
          done(err);
        }
      });
  }).timeout(1000);

  it("Stream empty array data", (done) => {
    const expectedBuffer = Buffer.from(JSON.stringify([]));
    let bytes = Buffer.from("");

    /* Prepare the stream */
    const readable = new Stream.Readable({ objectMode: true });

    /* Finish data emit */
    readable.push(null);
    readable
      .pipe(
        Streams.stringify((data: any) => {
          return { name: data.name };
        })
      )
      .on("data", (chunk: any) => {
        const chunkBuff = Buffer.from(chunk);
        bytes = Buffer.concat([bytes, chunkBuff]);
      })
      .on("end", () => {
        try {
          expect(bytes).to.deep.equal(expectedBuffer);
          done();
        } catch (err) {
          done(err);
        }
      });
  }).timeout(1000);

  it("Stream non JSON object", (done) => {
    const expectedBuffer = Buffer.from(JSON.stringify(["Alfa"]));
    let bytes = Buffer.from("");

    /* Prepare the stream */
    const readable = new Stream.Readable({ objectMode: true });
    readable.push("Alfa");

    /* Finish data emit */
    readable.push(null);
    readable
      .pipe(
        Streams.stringify((data: any) => {
          return data;
        })
      )
      .on("data", (chunk: any) => {
        const chunkBuff = Buffer.from(chunk);
        bytes = Buffer.concat([bytes, chunkBuff]);
      })
      .on("end", () => {
        try {
          expect(bytes.toString()).to.deep.equal(expectedBuffer.toString());
          done();
        } catch (err) {
          done(err);
        }
      });
  }).timeout(1000);
});
