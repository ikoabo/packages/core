/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "mocha";
import chai from "chai";

import { Logger, LOG_LEVEL } from "../src/index";
const expect = chai.expect;

describe("Logger api", () => {
  it("Log level debug", (done) => {
    Logger.setLogLevel(LOG_LEVEL.DEBUG);
    const logger: Logger = new Logger("Tester");
    
    logger.info("Hello world");
    logger.debug("Hello world");
    logger.warn("Hello world");
    logger.error("Hello world");

    expect(Logger.logLevel).to.be.a("string").to.be.eq(LOG_LEVEL.DEBUG);
    done();
  }).timeout(1000);
});
