/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Core Package
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */

/**
 * Predefined base status
 */
export enum SERVER_STATUS {
  SOFT_DELETE = -1,
  UNKNOWN = 0,
  DISABLED = 1,
  ENABLED = 2
}
